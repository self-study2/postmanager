@extends('layouts.app')

@section('content')
	
	@if(count($post->toArray()) > 0)
	<div class="row">
		@include("sections.post-section", ["post" => $post, "action" => "detail"])
	</div>
	@else
	<div class="row">
		<h3>Not found any post.</h3>
	</div>
	@endif
@endsection
