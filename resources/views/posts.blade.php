@extends('layouts.app')

@section('content')
	
	@if(count($posts) > 0)
	<div class="row">
		@foreach($posts as $post)	
			@include("sections.post-section", ["post" => $post, "action" => "home"])
		@endforeach
	</div>
	@else
	<div class="row">
		<h3>Not found any post.</h3>
	</div>
	@endif
@endsection
