@extends('layouts.app')

@section('content')
	<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create New Post') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('savePost') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="title">{{ __('Title') }}</label>
							<input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" placeholder="Enter title" autocomplete="title" autofocus>
							
							@error('title')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
                        </div>
						
                        <div class="form-group">
                            <label for="image">{{ __('Image') }}</label>
							<input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" autocomplete="image" autofocus>

							@error('image')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
                        </div>

                        <div class="form-group">
                            <label for="description">{{ __('Description') }}</label>
							<textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" placeholder="Enter title" autocomplete="description">
								{{ old('description') }}
							</textarea>

							@error('description')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
                        </div>

                        <div class="form-group mb-0">
							<button type="submit" class="btn btn-primary">
								{{ __('Save Post') }}
							</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
