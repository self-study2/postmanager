<!-- Post Content Column -->
<div class="col-lg-8">
	<!-- Title -->
	<h1 class="mt-4">
		<a href="{{ route('showPost', $post->id) }}">{{ __($post->title) }}</a>
	</h1>

	<!-- Author -->
	<p class="lead">
	  by
	  <a href="#">{{ __($post->user->name) }}</a>
	</p>

	<hr/>

	<!-- Date/Time -->
	<p>
		Posted on: {{ date("F d, Y H:i:A", strtotime($post->created_at)) }}
		<br>
		Total Comments: {{ $post->Comments->count() }}
	</p>

	<hr/>

	<!-- Preview Image -->
	<img class="img-fluid rounded" src="{{URL::to('/uploads/posts/'.$post->image)}}" alt="">

	<hr/>

	<!-- Post Content -->
	<p> {{ __($post->description) }} </p>

	<hr/>
	
	@if(!Auth::guest())
		@include("sections.post-comment-form", ['id' => $post->id])
	@endguest
	
	@include("sections.post-comments", ['comments' => $post->Comments, "action" => $action])
</div>