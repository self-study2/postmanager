<!-- Comments Form -->
<div class="card my-4">
	<h5 class="card-header">Leave a Comment:</h5>
	<div class="card-body">
		<form method="post" action="{{ route('saveComment') }}">
			@csrf
			<input type="hidden" name="post_id" value="{{ $id }}" />
			<div class="form-group">
				<textarea name="comment" class="form-control" rows="3"></textarea>
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
</div>