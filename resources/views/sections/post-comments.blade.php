<!-- Single Comment -->
@if(count($comments) > 0)
	@php
		$i = 1;
	@endphp
	@foreach($comments as $comment)
		<div class="media mb-4">
			<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
			<div class="media-body">
				<h5 class="mt-0">
					{{ $comment->user->name}}
					
					@if(!Auth::guest() && \Auth::user()->id == $comment->user_id)
						<button type="button" class="btn btn-xs btn-danger" onclick="event.preventDefault();document.getElementById('delete-comment-form-{{$comment->id}}').submit();">
							{{ __('Delete') }}
						</button>

						<form id="delete-comment-form-{{$comment->id}}" action="{{ route('deletComment', $comment->id) }}" method="POST" style="display: none;">
							@csrf
							<input type="hidden" name="comment_id" value="{{$comment->id}}"/>
						</form>
					@endif
				</h5>
				{{ __($comment->comment) }}
			</div>
		</div>
		@php
			if($action == "home" && $i == 5)
				break;
			
			$i++;
		@endphp
	@endforeach
@endif