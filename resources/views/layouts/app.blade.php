<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{ config('app.name', 'Laravel') }}</title>

		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}" defer></script>

		<!-- Fonts -->
		<link rel="dns-prefetch" href="//fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		
		<!-- Bootstrap core CSS -->
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link href="{{ asset('css/blog-post.css') }}" rel="stylesheet">
	</head>
	<body>
		@include("sections.header")
		
		<!-- Page Content -->
		<div class="container" id="app">
			@if(Session::has('success'))
				<div class="row infoWindow">
					<div class="col-lg-12">
						<div class="alert alert-info alert-dismissable" data-dismiss="alert" aria-hidden="true">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<i class="fa fa-info-circle"></i>  <strong>{!! session('success') !!}</strong> 
						</div>
					</div>
				</div>
				<script>
					setTimeout(function(){ $('.close').trigger('click'); },5000);
				</script>
			@endif
			
			@if(Session::has('error'))
				<div class="row">
					<div class="col-lg-12">
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<i class="fa fa-info-circle"></i>  <strong>{!! session('error') !!}</strong> 
						</div>
					</div>
				</div>
				
				<script>
					setTimeout(function(){ $('.close').trigger('click'); },5000);
				</script>
			@endif
			
			@yield("content")
		</div>
		<!-- /.container -->
		
		@include("sections.footer")
		
	</body>
</html>
