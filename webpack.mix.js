const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
.copy('resources/css/bootstrap.min.css', 'public/css/bootstrap.min.css')
.copy('resources/css/blog-post.css', 'public/css/blog-post.css')
.copy('resources/js/jquery.min.js', 'public/js/jquery.min.js')
.copy('resources/js/bootstrap.bundle.min.js', 'public/js/bootstrap.bundle.min.js')
.sass('resources/sass/app.scss', 'public/css');
