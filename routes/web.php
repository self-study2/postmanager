<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'PostController@index')->name('posts');
Route::get('/post/{id}', 'PostController@show')->name('showPost');
Route::get('/post/create', 'PostController@create')->name('createPost')->middleware('auth');
Route::post('/post/save', 'PostController@store')->name('savePost')->middleware('auth');
Route::post('/post/comment/save', 'PostController@storeComment')->name('saveComment')->middleware('auth');
Route::post('/post/comment/delete', 'PostController@deleteComment')->name('deletComment')->middleware('auth');


/* Display array in formated view */
function pr($data){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
}


\DB::enableQueryLog();
		
/* Display the query log */
function qLog(){
	$querydata =  (\DB::getQueryLog());
	pr($querydata);
}