<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'image', 'user_id'
    ];
	
	public function User(){
		return $this->belongsTo('\App\User','user_id');
	}
	
	public function Comments(){
		return $this->hasMany('\App\PostComment','post_id');
	}
}
