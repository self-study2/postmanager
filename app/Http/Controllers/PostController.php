<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/** Request File Validations **/
use App\Http\Validations\PostValidation;

use App\Post;
use App\PostComment;

use Image; 

class PostController extends Controller
{
    /**
     * Display a listing of the posts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$posts = Post::where('status', '1')->with(['User', 'Comments'])->get();
		//pr($posts);die;
        return view('posts', compact(['posts']));
    }

    /**
     * Show the form for creating a new post.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create-post-form');
    }

    /**
     * Store a newly created post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostValidation $request)
    {
        $data = $request->all();
		
		$post = new Post();
		
		/** Below code for save post image **/
		$destinationPath = public_path('/uploads/posts/');
		$newName = '';
		if ($request->hasFile('image')) {
			$fileName = $data['image']->getClientOriginalName();
			$file = request()->file('image');
			$fileNameArr = explode('.', $fileName);
			$fileNameExt = end($fileNameArr);
			$newName = date('His').rand() . time() . '.' . $fileNameExt;
			
			$file->move($destinationPath, $newName);
		}
		
		$post->title = $data['title'];
		$post->description = $data['description'];
		$post->image = $newName;
		$post->user_id = \Auth::user()->id;
		
		if($post->save()){
			\Session::flash('success', "your post created successfully.");
			return \Redirect::to('/');
		}
		else{
			\Session::flash('error', 'Something went wrong, the campaign not add ,please try again.');
			return \Redirect::to("/post/create");
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where('id', $id)->with(['User', 'Comments'])->first();
		return view('post-detail', compact(['post']));
    }

	public function storeComment(Request $request){
		$data = $request->all();
		
		$validArr = [
			'post_id' => 'required',
			'comment' => 'required'
		];
		
		$validation = Validator::make($data, $validArr);
		if ($validation->passes()) {
			$postComment = new PostComment();
			
			$postComment->user_id = \Auth::user()->id;
			$postComment->post_id = $data['post_id'];
			$postComment->comment = $data['comment'];
			
			if($postComment->save()){
				\Session::flash('success', 'comment save successfully.');
				return redirect()->back();
			}else{
				\Session::flash('error', 'comment not save.');
				return redirect('/admin/agents/add')->withErrors($validation)->withInput();
			}
		}
		else{
			\Session::flash('error', 'comment not save.');
			return redirect('/admin/agents/add')->withErrors($validation)->withInput();
		}
	}

	public function deleteComment(Request $request){
		$data = $request->all();
		
		$validArr = [
			'comment_id' => 'required'
		];
		
		$validation = Validator::make($data, $validArr);
		if ($validation->passes()) {
			$postComment = PostComment::where('id', $data['comment_id'])->first();
			
			if($postComment->user_id == \Auth::user()->id){
				PostComment::where('id', $data['comment_id'])->delete();
				
				\Session::flash('success', 'comment deleted successfully.');
				return redirect()->back();
			}else{
				\Session::flash('error', 'comment not delete, because this is not yours comment.');
				return redirect('/')->withErrors($validation)->withInput();
			}
		}
		else{
			\Session::flash('error', 'comment not deleted.');
			return redirect('/')->withErrors($validation)->withInput();
		}
	}
}
