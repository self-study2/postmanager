<?php

namespace App\Http\Validations;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PostValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
		$data = $request->all();
		//pr($data);die;
        $validate = [
            'title' => 'required|string|max:255',
			'description' => 'required',
			'image' => 'required|image|mimes:jpeg,jpg,png,gif'
        ];
		
		return $validate;
    }
	
	public function messages(){
		return [];
	}
}
